import React, { Component } from "react";

import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux'
import thunk from 'redux-thunk';
import { StyleSheet, View , Text , FlatList, StatusBar, Button, SafeAreaView, Dimensions, Image, ScrollView, ImageBackground, TouchableOpacity} from 'react-native'
import LinearGradient from 'react-native-linear-gradient';
import { withNavigation } from 'react-navigation';

import { addwishItems,clearwishList,cartItems} from '../../action';
import t from 'tcomb-form-native';
import {Collapse,CollapseHeader, CollapseBody, AccordionList} from 'accordion-collapse-react-native';

const Form = t.form.Form;


var Gender = t.enums({
  M: 'Male',
  F: 'Female'
});
// see the "Rendering options" section in this guide
var billing_options = {
  auto: 'placeholders',
	stylesheet: styles,
  fields: {
    birthDate: {
      mode: 'date' // display the Date field as a DatePickerAndroid
    },
    gender: {
      nullOption: {value: '', text: 'Choose your gender'}
    }
  }
};
var billing_details = t.struct({
  First_Name: t.String,
  Last_Name: t.String,
  Email: t.String,
  Telephone: t.Number,
  Company: t.maybe(t.String),
  Address: t.String,
  City: t.String,
  PostalCode: t.String,
  Country: t.String,
  State: t.String,
  usefordelivery: t.Boolean
  //  birthDate: t.Date,
  // gender: Gender // enum
});


var delivery_options = {
  auto: 'placeholders',
	stylesheet: styles,
  fields: {
    birthDate: {
      mode: 'date' // display the Date field as a DatePickerAndroid
    },
    gender: {
      nullOption: {value: '', text: 'Choose your gender'}
    }
  }
};
var delivery_details = t.struct({
  First_Name: t.String,
  Last_Name: t.String,
  usefordelivery: t.Boolean
});

class CheckOut extends Component {
    constructor(props) {
    super(props);
    
    
  }
  state = {
    collapsed:false
    }
  static navigationOptions={
    drawerIcon:()=>(
    <Icon style={{fontSize:24,color:"#000" }} name="ios-heart" />
    )
}

componentDidMount() {
  // give focus to the name textbox
 
}

onChange(value) {
  // recalculate the type only if strictly necessary
  let type = value.usefordelivery;
if(type){
this.setState({collapsed:true})
}
}
_onPress= () =>{
  
 
  var value = this.refs.form.getValue();
  if (value) {
    //console.log(value);
  }
}
_processcart = (array) =>{
 const result = [...array.reduce((r, e) => {
  let k = `${e.product_id}|${e.product_name}`;
  if(!r.has(k)) r.set(k, {...e, count: 1})
  else r.get(k).count++
  return r;
}, new Map).values()]


return result;

//console.log(array);
}



    render() {
     
        return (
            <ScrollView>
              <Text style={{textAlign: 'center',fontSize: 22,fontWeight:'bold',paddingVertical: 18 ,color:'#000'  }}>CHECKOUT</Text>
              
              <Collapse disabled={this.state.collapsed}  style={{marginTop:1}}>
    <CollapseHeader >
    <LinearGradient start={{x: 0, y: 0}} end={{x: 0, y: 1}} locations={[.1,.6]} colors={['#FF0000', '#6C0707']} style={{flex:1}}>
      <View>
        <Text style={styles.heading}>BILLING DETAILS</Text>
      </View>
      </LinearGradient>
    </CollapseHeader>
    <CollapseBody style={{paddingHorizontal:20,paddingVertical:10}}>
     

             <View style={styles.container}>
                <Form type={billing_details} options={billing_options} />
              </View>
        <TouchableOpacity style={styles.button} onPress={() => {alert('working')}}>
                <Text style={styles.buttonText}>UPDATE</Text>
                </TouchableOpacity>
    </CollapseBody>
</Collapse>


<Collapse  style={{marginTop:1}}>
    <CollapseHeader>
    <LinearGradient start={{x: 0, y: 0}} end={{x: 0, y: .6}} locations={[.1,1]} colors={['#FF0000', '#6C0707']} style={{flex:1}}>
      <View>
        <Text style={styles.heading}>DELIVERY DETAILS</Text>
      </View>
      </LinearGradient>
    </CollapseHeader>
    <CollapseBody style={{paddingHorizontal:20,paddingVertical:10}}>
     

             <View style={styles.container}>
          
                <Form ref="form" onChange={this.onChange.bind(this)} type={delivery_details} options={delivery_options} />
              </View>
        <TouchableOpacity style={styles.button} onPress={this._onPress}>
                <Text style={styles.buttonText}>UPDATE</Text>
                </TouchableOpacity>
    </CollapseBody>
</Collapse>

<Collapse onToggle={()=>alert('ok')} style={{marginTop:1}}>
    <CollapseHeader>
    <LinearGradient start={{x: 0, y: 0}} end={{x: 0, y: .6}} locations={[.1,1]} colors={['#FF0000', '#6C0707']} style={{flex:1}}>
      <View>
        <Text style={styles.heading}>DELIVERY METHOD</Text>
      </View>
      </LinearGradient>
    </CollapseHeader>
    <CollapseBody style={{paddingHorizontal:20,paddingVertical:10}}>
     

             <View style={styles.container}>
                <Form type={delivery_details} options={delivery_options} />
              </View>
        <TouchableOpacity style={styles.button} onPress={() => {alert('working')}}>
                <Text style={styles.buttonText}>UPDATE</Text>
                </TouchableOpacity>
    </CollapseBody>
</Collapse>


<Collapse style={{marginTop:1}}>
    <CollapseHeader>
    <LinearGradient start={{x: 0, y: 0}} end={{x: 0, y: .6}} locations={[.1,1]} colors={['#FF0000', '#6C0707']} style={{flex:1}}>
      <View>
        <Text style={styles.heading}>PAYMENT METHOD</Text>
      </View>
      </LinearGradient>
    </CollapseHeader>
    <CollapseBody style={{paddingHorizontal:20,paddingVertical:10}}>
     

             <View style={styles.container}>
                <Form type={delivery_details} options={delivery_options} />
              </View>
        <TouchableOpacity style={styles.button} onPress={() => {alert('working')}}>
                <Text style={styles.buttonText}>UPDATE</Text>
                </TouchableOpacity>
    </CollapseBody>
</Collapse>

<Collapse style={{marginTop:1}}>
    <CollapseHeader>
    <LinearGradient start={{x: 0, y: 0}} end={{x: 0, y: .6}} locations={[.1,1]} colors={['#FF0000', '#6C0707']} style={{flex:1}}>
      <View>
        <Text style={styles.heading}>CONFIRM ORDER</Text>
      </View>
      </LinearGradient>
    </CollapseHeader>
    <CollapseBody style={{paddingHorizontal:20,paddingVertical:10}}>
     

             <View style={styles.container}>
                <Form type={delivery_details} options={delivery_options} />
              </View>
        <TouchableOpacity style={styles.button} onPress={() => {alert('working')}}>
                <Text style={styles.buttonText}>UPDATE</Text>
                </TouchableOpacity>
    </CollapseBody>
</Collapse>
             </ScrollView>
        );
    }
}



export default connect(null,null)(withNavigation(CheckOut));


const styles = StyleSheet.create({
  container: {
    flex: 1
   
  },
heading:{
paddingVertical:20,
paddingHorizontal:10,
color:'#FFF',
fontWeight:'bold'
},
  item: {
    backgroundColor: '#fff',
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    margin: 1,
    height: 70, // approximate a square
  },
  itemInvisible: {
    backgroundColor: 'transparent',
  },
  itemText: {
    color: '#000',
    fontWeight: 'normal',
    fontSize:10,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    flex:1,
    overflow:'hidden' 
  },
  count:{
    flex:1,
   color: 'red',
    fontWeight: 'normal',
    fontSize:18,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  
  },
button:{
borderWidth:1,
       borderColor:'rgba(0,0,0,0.2)',
       alignItems:'center',
       justifyContent:'center',
       width:150,
       height:40,
       backgroundColor:'#fff',
       marginHorizontal: 5,
       padding:4,
       borderRadius:4,
       flexDirection:"row",
       flex:1
},
addbutton:{
  color: 'green',
  borderColor:'green'
},
buttonwrap:{
flex:1,
flexDirection: 'row',
alignItems:  'center' ,
justifyContent:  'center' 
},
  contentContainer: {
    paddingVertical: 0
  },
  bottom: {
    flex: 1,
    justifyContent: 'flex-end',
    
  }
})

  // <TouchableOpacity onPress={() => this.props.removeItem({"product_id":item.product_id})} style={{flex: 1}}><Icon  name="md-close" size={20} /></TouchableOpacity>